// BEGIN
const faker = require('faker');
require('expect-puppeteer');

const appUrl = 'http://localhost:5000';

beforeAll(async () => {
  await page.goto(appUrl);
});

test('Shows main page.', async () => {
  await page.goto(appUrl);

  await expect(page).toMatch('Welcome to a Simple blog!');
});

test('Shows the list of articles.', async () => {
  await page.goto(`${appUrl}/articles`);

  const listItems = await page.$$('[data-testid="article-name"]');

  expect(listItems.length).toBeGreaterThan(0);
});

test('Shows new article form.', async () => {
  await page.goto(`${appUrl}/articles`);

  await page.click('[data-testid="article-create-link"]');

  await expect(page).toMatchElement('[data-testid="new-article-form"]');
});

test('Creates a new article.', async () => {
  await page.goto(`${appUrl}/articles/new`);

  const articleName = faker.name.title();

  await expect(page).toFillForm('form[data-testid="new-article-form"]', {
    'article[name]': articleName,
    'article[content]': faker.lorem.paragraph(),
  });

  await expect(page).toSelect('[name="article[categoryId]"]', '1');

  await expect(page).toClick('[data-testid="article-create-button"]');

  await page.waitForSelector('[data-testid="articles"]');

  await expect(page).toMatch(articleName);
});

test('Edits an article.', async () => {
  await page.goto(`${appUrl}/articles/new`);

  const articleName = faker.name.title();

  await expect(page).toFillForm('form[data-testid="new-article-form"]', {
    'article[name]': articleName,
    'article[content]': faker.lorem.paragraph(),
  });

  await expect(page).toSelect('[name="article[categoryId]"]', '1');

  await expect(page).toClick('[data-testid="article-create-button"]');

  await page.waitForSelector('[data-testid="articles"]');

  const articleId = await page.evaluate((elem) => elem.innerText, (await page.$('[data-testid="articles"] [data-testid="article"]:last-child [data-testid="article-id"]')));

  await page.goto(`${appUrl}/articles/${articleId}/edit`);

  const newArticleName = faker.name.title();

  await expect(page).toFill('#name', newArticleName);

  await page.click('[data-testid="article-update-button"]');

  await page.waitForSelector('[data-testid="articles"]');

  await expect(page).toMatch(newArticleName);
});
// END
