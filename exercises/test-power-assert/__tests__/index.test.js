const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const array = [1, [2, [3, [4]], 5]];

assert.deepEqual(flattenDepth(array, 2), [1, 2, 3, [4], 5], 'Flattens the array for two levels.');

assert.deepEqual(flattenDepth([]), [], 'Returns initial array.');

assert.deepEqual(flattenDepth(array, 1), [1, 2, [3, [4]], 5], 'Flattens the array for one level by default.');

assert.deepEqual(flattenDepth(array, 10), [1, 2, 3, 4, 5], 'Returns flat array.');

assert.deepEqual(flattenDepth(array, 0), array, 'Returns initial array.');

assert.deepEqual(flattenDepth(array, -1), array, 'Returns initial array.');
// END
