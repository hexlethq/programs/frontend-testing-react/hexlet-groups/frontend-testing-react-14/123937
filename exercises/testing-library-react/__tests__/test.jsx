// @ts-check

import '@testing-library/jest-dom';

import nock from 'nock';
import React from 'react';
import countryList from 'country-list';
import { render, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Autocomplete from '../src/Autocomplete.jsx';

const countries = countryList.getNames();

const getCountries = (term) => {
  const result = countries.filter((c) => c.toLowerCase().startsWith(term.toLowerCase()));

  return result;
};

const host = 'http://localhost';

beforeAll(() => {
  nock.disableNetConnect();
});

const terms = ['a', 'al', 'alb'];

// BEGIN
test('Shows correct countries list.', async () => {
  terms.forEach((term) => {
    nock(host)
      .get('/countries')
      .query({ term })
      .reply(200, getCountries(term));
  });

  const { container, getByRole, queryByText } = render(<Autocomplete />);

  const textField = getByRole('textbox');

  userEvent.type(textField, 'a');

  await waitFor(() => expect(queryByText(/afghanistan/i)).toBeVisible());
  await waitFor(() => expect(queryByText(/albania/i)).toBeVisible());
  await waitFor(() => expect(queryByText(/algeria/i)).toBeVisible());

  userEvent.type(textField, 'l');

  await waitFor(() => expect(queryByText(/afghanistan/i)).toBeNull());
  await waitFor(() => expect(queryByText(/albania/i)).toBeVisible());
  await waitFor(() => expect(queryByText(/algeria/i)).toBeVisible());

  userEvent.type(textField, 'b');

  await waitFor(() => expect(queryByText(/afghanistan/i)).toBeNull());
  await waitFor(() => expect(queryByText(/algeria/i)).toBeNull());
  await waitFor(() => expect(queryByText(/albania/i)).toBeVisible());

  userEvent.clear(textField);

  expect(textField).toHaveValue('');
  expect(container.querySelector('ul')).toBeNull();
});
// END
