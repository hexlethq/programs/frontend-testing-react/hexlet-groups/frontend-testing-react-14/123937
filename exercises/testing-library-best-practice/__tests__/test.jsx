// @ts-check

import React from 'react';
import nock from 'nock';
import axios from 'axios';
import httpAdapter from 'axios/lib/adapters/http';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import faker from 'faker';
import { v4 as uuidV4 } from 'uuid';

import TodoBox from '../src/TodoBox.jsx';

axios.defaults.adapter = httpAdapter;
const host = 'http://localhost';

const buildTask = (props = {}) => {
  const {
    id = uuidV4(),
    text = faker.lorem.word(),
    state = 'active',
  } = props;

  return { id, text, state };
};

const renderComponent = () => render(<div className="wrapper"><TodoBox /></div>);

beforeAll(() => {
  nock.disableNetConnect();
});

beforeEach(() => {
  nock(host)
    .get('/tasks')
    .reply(200, []);
});

afterEach(() => {
  nock.cleanAll();
});

afterAll(() => {
  nock.restore();
  nock.enableNetConnect();
});

const getTaskField = () => screen.getByRole('textbox');

const getTaskButton = () => screen.getByRole('button', { name: /add/i });

const tasksContainerSelectors = {
  active: '.todo-active-tasks',
  finished: '.todo-finished-tasks',
};

const getTasksContainer = (tasksType) => {
  const selector = tasksContainerSelectors[tasksType];
  const field = getTaskField();
  const container = field.closest('.wrapper');
  const tasksContainer = container.querySelector(selector);

  return tasksContainer;
};

const getActiveTasksContainer = () => getTasksContainer('active');

const getFinishedTasksContainer = () => getTasksContainer('finished');

const createTask = async (taskText) => {
  userEvent.type(getTaskField(), taskText);
  userEvent.click(getTaskButton());

  await screen.findByText(taskText);
};

const toggleTask = async (taskText) => {
  userEvent.click(screen.getByRole('link', { name: new RegExp(taskText) }));
};

// BEGIN
test('Shows the app initial state.', async () => {
  renderComponent();

  await waitFor(() => expect(getTaskField()).toBeInTheDocument());
  await waitFor(() => expect(getTaskField()).toHaveValue(''));
  await waitFor(() => expect(getTaskButton()).toBeInTheDocument());
  await waitFor(() => expect(getActiveTasksContainer()).not.toBeInTheDocument());
  await waitFor(() => expect(getFinishedTasksContainer()).not.toBeInTheDocument());
});

test('Creates tasks.', async () => {
  const taskText1 = faker.lorem.word();
  const taskText2 = faker.lorem.word();

  nock(host)
    .persist()
    .post('/tasks')
    .reply(200, (uri, reqBody) => buildTask({ text: reqBody.text }));

  renderComponent();

  await createTask(taskText1);
  await createTask(taskText2);

  expect(screen.getByText(taskText1)).toBeInTheDocument();
  expect(screen.getByText(taskText2)).toBeInTheDocument();
});

test('Updates tasks.', async () => {
  const task = buildTask();

  nock(host)
    .persist()
    .post('/tasks')
    .reply(200, task);

  nock(host)
    .patch(`/tasks/${task.id}/finish`)
    .reply(200, { ...task, state: 'finished' });

  nock(host)
    .patch(`/tasks/${task.id}/activate`)
    .reply(200, { ...task, state: 'active' });

  renderComponent();

  await createTask(task.text);

  expect(screen.getByText(task.text)).toBeInTheDocument();

  await toggleTask(task.text);

  await waitFor(() => expect(getActiveTasksContainer()).not.toBeInTheDocument());
  await waitFor(() => expect(getFinishedTasksContainer()).toBeInTheDocument());

  await toggleTask(task.text);

  await waitFor(() => expect(getActiveTasksContainer()).toBeInTheDocument());
  await waitFor(() => expect(getFinishedTasksContainer()).not.toBeInTheDocument());
});
// END
