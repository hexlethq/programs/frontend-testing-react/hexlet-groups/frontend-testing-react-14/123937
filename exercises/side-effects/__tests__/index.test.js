const os = require('os');
const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

const fixtureName = 'package.json';

const getFixturePath = (filename) => path.join('__fixtures__', filename);

const readFile = (filename) => fs.readFileSync(filename, 'utf-8');

let tmpDir;

// BEGIN
describe('upVersion', () => {
  beforeEach(() => {
    tmpDir = fs.mkdtempSync(path.join(os.tmpdir(), 'upVersion'));

    fs.copyFileSync(getFixturePath(fixtureName), path.join(tmpDir, fixtureName));
  });

  test('Updates patch version by default.', () => {
    const packageFilePath = path.join(tmpDir, fixtureName);

    upVersion(packageFilePath);

    const rawData = readFile(packageFilePath);

    const data = JSON.parse(rawData);

    expect(data).toEqual({ version: '1.3.3' });
  });

  test('Updates minor version.', () => {
    const packageFilePath = path.join(tmpDir, fixtureName);

    upVersion(packageFilePath, 'minor');

    const rawData = readFile(packageFilePath);

    const data = JSON.parse(rawData);

    expect(data).toEqual({ version: '1.4.0' });
  });

  test('Updates major version.', () => {
    const packageFilePath = path.join(tmpDir, fixtureName);

    upVersion(packageFilePath, 'major');

    const rawData = readFile(packageFilePath);

    const data = JSON.parse(rawData);

    expect(data).toEqual({ version: '2.0.0' });
  });
});
// END
