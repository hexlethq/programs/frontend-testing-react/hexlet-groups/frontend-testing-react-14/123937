// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');
const faker = require('faker');

const run = require('../src/application');

const { screen, configure } = testingLibraryDom;
const userEvent = testingLibraryUserEvent.default;

configure({
  testIdAttribute: 'data-container',
});

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

const createList = (listName) => {
  userEvent.type(screen.getByRole('textbox', { name: /new list name/i }), listName);
  userEvent.click(screen.getByRole('button', { name: /add list/i }));
};

const createTask = (taskName) => {
  userEvent.type(screen.getByRole('textbox', { name: /new task name/i }), taskName);
  userEvent.click(screen.getByRole('button', { name: /add task/i }));
};

// BEGIN
test('Shows the application.', () => {
  expect(screen.getByTestId('tasks')).toBeEmptyDOMElement();
  expect(screen.getByText(/general/i)).toBeVisible();
});

test('Adds tasks to the default list by default.', () => {
  const task1 = faker.lorem.word();
  const task2 = faker.lorem.word();

  createTask(task1);
  createTask(task2);

  expect(screen.getByText(/general/i)).toBeVisible();
  expect(screen.getByText(new RegExp(task1))).toBeVisible();
  expect(screen.getByText(new RegExp(task2))).toBeVisible();
  expect(screen.getByRole('textbox', { name: /new task name/i })).toHaveValue('');
});

test('Adds a new list with tasks.', () => {
  const secondaryListName = 'Secondary';

  const task1 = faker.lorem.word();
  const task2 = faker.lorem.word();

  createList(secondaryListName);

  createTask(task1);

  expect(screen.getByText(new RegExp(task1))).toBeVisible();

  userEvent.click(screen.getByRole('link', { name: new RegExp(secondaryListName) }));

  expect(screen.getByTestId('tasks')).toBeEmptyDOMElement();

  createTask(task2);

  expect(screen.getByText(new RegExp(task2))).toBeVisible();
});
// END
