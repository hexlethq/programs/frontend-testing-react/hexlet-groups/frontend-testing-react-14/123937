const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

nock.disableNetConnect();

axios.defaults.adapter = require('axios/lib/adapters/http');

const users = [{ id: '1', name: 'John' }, { id: '2', name: 'Smith' }];

// BEGIN
describe('User CRUD operations', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  afterAll(() => {
    nock.restore();
  });

  test('Makes get request.', async () => {
    const scope = nock('https://example.com')
      .get('/users')
      .reply(200, users);

    const response = await get('https://example.com/users');

    expect(scope.isDone()).toBeTruthy();
    expect(response).toMatchObject(
      expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(String),
          name: expect.any(String),
        }),
      ]),
    );
  });

  test('Handles an error during get request.', async () => {
    const scope = nock('https://example.com')
      .get('/users')
      .reply(404);

    await expect(get('https://example.com/users')).rejects.toThrow();
    expect(scope.isDone()).toBeTruthy();
  });

  test('Makes post request.', async () => {
    const body = {
      firstname: 'Fedor',
      lastname: 'Sumkin',
      age: 33,
    };

    const expectedUser = { ...body, id: '1' };

    const scope = nock('https://example.com')
      .post('/users', body)
      .reply(200, expectedUser);

    const response = await post('https://example.com/users', body);

    expect(response).toMatchObject(expectedUser);
    expect(scope.isDone()).toBeTruthy();
  });

  test('Handles an error during post request.', async () => {
    const body = {
      firstname: 'Fedor',
      lastname: 'Sumkin',
      age: 33,
    };

    const scope = nock('https://example.com')
      .post('/users', body)
      .reply(500);

    await expect(post('https://example.com/users', body)).rejects.toThrow();
    expect(scope.isDone()).toBeTruthy();
  });
});
// END
