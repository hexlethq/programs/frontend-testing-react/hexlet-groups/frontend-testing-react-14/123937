const axios = require('axios');

// BEGIN
const get = async (url) => {
  const response = await axios.get(url);

  return response.data;
};

const post = async (url, body) => {
  const response = await axios.post(url, body);

  return response.data;
};
// END

module.exports = { get, post };
