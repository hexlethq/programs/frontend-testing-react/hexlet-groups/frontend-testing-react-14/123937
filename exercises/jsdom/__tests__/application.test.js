// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const faker = require('faker');

const run = require('../src/application');

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
const defaultListId = 'general';

const getTasksContainer = () => document.querySelector('[data-container="tasks"]');
const getListsContainer = () => document.querySelector('[data-container="lists"]');
const getListByIndex = (container, index) => {
  const lists = container.querySelectorAll('li');
  const result = lists[index];

  return result;
};
const getTaskInput = () => document.querySelector('[data-testid="add-task-input"]');
const getTaskForm = () => document.querySelector('[data-container="new-task-form"]');
const getListInput = () => document.querySelector('[data-testid="add-list-input"]');
const getListForm = () => document.querySelector('[data-container="new-list-form"]');
const getListLinkById = (listId) => document.querySelector(`[data-testid="${listId}-list-item"]`);
const createTask = (taskName) => {
  getTaskInput().value = taskName;
  getTaskForm().submit();
};
const createList = (listName) => {
  getListInput().value = listName;
  getListForm().submit();
};

test('Shows one default list and does not show tasks.', () => {
  const listsContainer = getListsContainer();
  const defaultList = getListByIndex(listsContainer, 0);

  expect(getTasksContainer()).toBeEmptyDOMElement();
  expect(defaultList).toHaveTextContent(/General/);
});

test('Adds tasks to the default list.', () => {
  const task1 = faker.lorem.word();
  const task2 = faker.lorem.word();
  const task3 = faker.lorem.word();
  const tasks = [task1, task2, task3];

  tasks.forEach(createTask);

  expect(getTaskInput().value).toBe('');
  expect(getTasksContainer()).toHaveTextContent(new RegExp(task1));
  expect(getTasksContainer()).toHaveTextContent(new RegExp(task2));
  expect(getTasksContainer()).toHaveTextContent(new RegExp(task3));
});

test('Creates a second list with tasks.', () => {
  const defaultListTasks = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];
  const customListTasks = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];

  const customListName = faker.lorem.word();

  createList(customListName);

  defaultListTasks.forEach(createTask);

  getListLinkById(customListName).dispatchEvent(new Event('click'));

  customListTasks.forEach(createTask);

  expect(getListInput().value).toBe('');
  expect(getListsContainer()).toHaveTextContent(new RegExp(customListName));

  customListTasks.forEach((taskName) => {
    expect(getTasksContainer()).toHaveTextContent(new RegExp(taskName));
  });

  getListLinkById(defaultListId).dispatchEvent(new Event('click'));

  defaultListTasks.forEach((taskName) => {
    expect(getTasksContainer()).toHaveTextContent(new RegExp(taskName));
  });
});
// END
