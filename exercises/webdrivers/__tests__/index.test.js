const puppeteer = require('puppeteer');
const faker = require('faker');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;

let browser;
let page;

const app = getApp();

describe('it works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });
  // BEGIN
  test('Shows main page.', async () => {
    await page.goto(appUrl);

    const title = await page.$('#title');

    const pageTitle = await page.evaluate((element) => element.innerText, title);

    expect(pageTitle).toContain('Welcome to a Simple blog!');
  });

  test('Shows the list of articles.', async () => {
    await page.goto(`${appUrl}/articles`);

    const listItems = await page.$$('#articles tr');

    expect(listItems.length).toBeGreaterThan(0);
  });

  test('Shows new article form.', async () => {
    await page.goto(`${appUrl}/articles`);

    await page.click('[data-testid="newArticleButton"]');

    await expect(page.waitForSelector('[data-testid="newArticleForm"]')).resolves.toBeTruthy();
  });

  test('Creates a new article.', async () => {
    await page.goto(`${appUrl}/articles/new`);

    const articleName = faker.name.title();

    await (await page.$('#name')).type(articleName);
    await (await page.$('#category')).select('1');
    await (await page.$('#content')).type(faker.lorem.paragraph());

    await page.click('[data-testid="newArticleFormSubmit"]');

    await page.waitForSelector('#articles');

    const createdArticleName = await page.evaluate((elem) => elem.innerText, (await page.$('#articles tbody [data-testid="article"]:last-child [data-testid="articleName"]')));

    expect(createdArticleName).toContain(articleName);
  });

  test('Edits an article.', async () => {
    await page.goto(`${appUrl}/articles/new`);

    await (await page.$('#name')).type(faker.name.title());
    await (await page.$('#category')).select('1');
    await (await page.$('#content')).type(faker.lorem.paragraph());

    await page.click('[data-testid="newArticleFormSubmit"]');

    await page.waitForSelector('#articles');

    const articleId = await page.evaluate((elem) => elem.innerText, (await page.$('#articles tbody [data-testid="article"]:last-child [data-testid="articleId"]')));

    await page.goto(`${appUrl}/articles/${articleId}/edit`);

    const newArticleName = faker.name.title();

    await (await page.$('#name')).type(newArticleName);

    await page.click('[data-testid="editArticleFormSubmit"]');

    await page.waitForSelector('#articles');

    const editedArticleName = await page.evaluate((elem) => elem.innerText, (await page.$('#articles tbody [data-testid="article"]:last-child [data-testid="articleName"]')));

    expect(editedArticleName).toContain(newArticleName);
  });

  // END
  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
