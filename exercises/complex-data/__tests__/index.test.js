const faker = require('faker');

// BEGIN
describe('createTransaction', () => {
  test('Generates valid transaction.', () => {
    expect(faker.helpers.createTransaction()).toMatchObject({
      amount: expect.stringMatching(/^[0-9]+(\.[0-9]{1,2})?$/gm),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.stringMatching(/[0-9]/),
    });
  });

  test('Generates unique transactions.', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();

    expect(transaction1).not.toMatchObject(transaction2);
    expect(transaction1).not.toBe(transaction2);
  });
});
// END
