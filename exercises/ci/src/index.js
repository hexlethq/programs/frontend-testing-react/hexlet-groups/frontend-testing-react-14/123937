const path = require('path');
const fs = require('fs');

const versionChunkNames = {
  patch: ([major, minor, patch]) => [major, minor, patch + 1].join('.'),
  minor: ([major, minor]) => [major, minor + 1, 0].join('.'),
  major: ([major]) => [major + 1, 0, 0].join('.'),
};

const getNextVersionFnSelector = (versionChunkName) => {
  const result = versionChunkNames[versionChunkName];

  if (!result) {
    throw new Error(`Invalid version chunk name "${versionChunkName}".`);
  }

  return result;
};

// BEGIN
const upVersion = (filepath, versionChunkName = 'patch') => {
  const resolvedPath = path.resolve(process.cwd(), filepath);
  const rawData = fs.readFileSync(resolvedPath, 'utf-8');
  const data = JSON.parse(rawData);
  const { version } = data;
  const versionChunks = version.split('.').map(Number);

  const getNextVersion = getNextVersionFnSelector(versionChunkName);
  const nextVersion = getNextVersion(versionChunks);

  const newData = JSON.stringify({ ...data, version: nextVersion });

  fs.writeFileSync(resolvedPath, newData, 'utf-8');
};
// END

module.exports = { upVersion };
