// @ts-check
// BEGIN
import _ from 'lodash';
import faker from 'faker';
import 'expect-puppeteer';

const appUrl = 'http://localhost:8080';

beforeAll(async () => {
  await page.goto(appUrl);
});

afterEach(async () => {
  await page.evaluate(() => {
    const { worker } = window.msw;

    worker.resetHandlers();
  });
});

test('Opens the app.', async () => {
  await page.goto(appUrl);

  await expect(page).toMatchElement('[data-testid="task-name-input"]');
  await expect(page).toMatchElement('[data-testid="add-task-button"]');
});

test('Adds a new task.', async () => {
  await page.goto(appUrl);

  await page.evaluate((taskId) => {
    const { worker, rest } = window.msw;

    worker.use(
      rest.post('/tasks', (req, res, ctx) => {
        const task = { ...req.body.task, id: taskId, state: 'active' };

        return res(
          ctx.json(task),
        );
      }),
    );
  }, _.uniqueId());

  const task = faker.lorem.word();

  await expect(page).toFill('[data-testid="task-name-input"]', task);

  await page.click('[data-testid="add-task-button"]');

  await expect(page).toMatch(task);
});

test('Shows an error message if there was an error during task creation.', async () => {
  await page.goto(appUrl);

  await page.evaluate(() => {
    const { worker, rest } = window.msw;

    worker.use(
      rest.post('/tasks', (req, res, ctx) => res(
        ctx.status(500),
      )),
    );
  });

  await expect(page).toFill('[data-testid="task-name-input"]', faker.lorem.word());

  await page.click('[data-testid="add-task-button"]');

  await expect(page).toMatch('Request failed with status code 500');
});

// It would be cool to test if user see the preloader (in case of slow network)
// but this application does not have preloader :((

test('Removes a task.', async () => {
  await page.goto(appUrl);

  const taskId = _.uniqueId();
  const taskName = faker.lorem.word();

  await page.evaluate((id) => {
    const { worker, rest } = window.msw;

    worker.use(
      rest.post('/tasks', (req, res, ctx) => {
        const task = { ...req.body.task, id, state: 'active' };

        return res(
          ctx.json(task),
        );
      }),

      rest.delete(`/tasks/${id}`, (req, res, ctx) => res(
        ctx.status(204),
      )),
    );
  }, taskId);

  await expect(page).toFill('[data-testid="task-name-input"]', taskName);

  await page.click('[data-testid="add-task-button"]');

  await expect(page).toMatch(taskName);

  await page.click(`[data-testid="remove-task-${taskId}"]`);

  await expect(page).not.toMatch(taskName);
});

// Here we should also check that we handle the case
// when an error occurred during task removing
// but this application does not have such functionality :((

// END
