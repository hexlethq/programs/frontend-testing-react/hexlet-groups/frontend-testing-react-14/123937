const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('sort', () => {
  test('Does not change amount of elements after sorting.', () => {
    fc.assert(fc.property(fc.array(fc.anything()), (list) => {
      const result = sort(list);

      expect(result.length).toBe(list.length);
    }));
  });

  test('Sorts correctly in case of double sorting.', () => {
    fc.assert(fc.property(fc.array(fc.anything()), (list) => {
      const result = sort(sort(list));

      expect(result).toEqual(sort(list));
    }));
  });

  test('Verifies that each subsequent element is less or equal to the previous element.', () => {
    fc.assert(fc.property(fc.array(fc.integer()), (list) => {
      const result = sort(list);

      expect(result).toBeSorted();
    }));
  });
});
// END
